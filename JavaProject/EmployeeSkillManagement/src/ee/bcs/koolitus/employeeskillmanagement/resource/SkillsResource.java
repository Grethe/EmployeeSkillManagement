package ee.bcs.koolitus.employeeskillmanagement.resource;

import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ee.bcs.koolitus.employeeskillmanagement.dao.Skills;

public abstract class SkillsResource {

	// list of skills
	public static List<Skills> getAllSkills() {
		List<Skills> skill = new ArrayList<>();
		String sqlQuery = "SELECT * FROM skills";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Skills skills = new Skills().setId(results.getInt("id"))
						.setSkill(results.getString("skill"))
						.setSkillGroup(SkillGroupResource.getSkillGroupById(results.getInt("skill_group_id")));
		
						//.setSkillGroupId(results.getInt("skill_group_id"));

				skill.add(skills);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set:" + e.getMessage());
		}

		return skill;
	}

	// adding new skills
	public static Skills addSkills(Skills skill) {
		String sqlQuery = "INSERT INTO skills (skill, skill_group_id) " + "VALUES ('" + skill.getSkill() + "', '"
				+ skill.getSkillGroup().getId() + "')";

		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				skill.setId(resultSet.getInt(1));
			}

		} catch (SQLException e) {
			System.out.println("Error on adding skill: " + e.getMessage());
		}
		return skill;

	}

	// searching for skills (by skill group)
	public static Set<Skills> getSkillsByGroupId(int skillGroupId) {
		Set<Skills> skillsByGroupId = new HashSet<>();
		String sqlQuery = "SELECT * FROM skills WHERE skill_group_id=" + skillGroupId;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				Skills skills = new Skills().setId(results.getInt("id")).setSkill(results.getString("skill"))
						.setSkillGroup(SkillGroupResource.getSkillGroupById(results.getInt("skill_group_id")));
						//.setSkillGroupId(results.getInt("skill_group_id"));
				
				skillsByGroupId.add(skills);

			}
		} catch (SQLException e) {
			System.out.println("Error on getting skill set:" + e.getMessage());
		}

		return skillsByGroupId;
	}
	
	//deleting skills 
	public static void deleteSkill(Skills skill) {   
		String sqlQuery = "DELETE from skills WHERE id=" + skill.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery); 
				if(code!=1) {
					throw new SQLException("Something went wrong while deleting skill");
				}

		} catch (SQLException e) {
			System.out.println("Error on getting skill set:" + e.getMessage());
		}
	}
	
	// modifying skills
	public static void updateSkills(Skills skill) {  
		String sqlQuery = "UPDATE skills SET skill='" + skill.getSkill() 
		+ "', skill_group_id='" + skill.getSkillGroup().getId() + "' WHERE id = " + skill.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery); 
				if(code!=1) {
					throw new SQLException("Something went wrong on updating skill");
				}

		} catch (SQLException e) {
			System.out.println("Error on getting skill set:" + e.getMessage());
		}
	}
	
	//get skill by ID
	public static Skills getSkillById (int skillId) {
		Skills skill = null;
		String sqlQuery = "SELECT * FROM skills WHERE id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, skillId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				
				skill = new Skills()
						.setId(results.getInt("id"))
						.setSkill(results.getString("skill"))
						.setSkillGroup(SkillGroupResource.getSkillGroupById(results.getInt("skill_group_id")));
						
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set: " + e.getMessage());
		}

		return skill;
	}

	
	}




