package ee.bcs.koolitus.employeeskillmanagement.resource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.employeeskillmanagement.dao.SkillGroup;

public abstract class SkillGroupResource {

	public static List<SkillGroup> getAllSkillGroups() {
		List<SkillGroup> skillGroup = new ArrayList<>();
		String sqlQuery = "SELECT * FROM skill_groups";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				SkillGroup skillGroups = new SkillGroup().setId(results.getInt("id"))
						.setSkillGroup(results.getString("skill_group"));
				skillGroup.add(skillGroups);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting skill group set:" + e.getMessage());
		}

		return skillGroup;
	}

	public static SkillGroup addSkillGroup(SkillGroup skillGroup) {
		String sqlQuery = "INSERT INTO skill_groups (skill_group) " + "VALUES ('" + skillGroup.getSkillGroup() + "')";

		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				skillGroup.setId(resultSet.getInt(1));
			}

		} catch (SQLException e) {
			System.out.println("Error on adding skill group: " + e.getMessage());
		}
		return skillGroup;

	}

	public static void deleteSkillGroup(SkillGroup skillGroup) {
		String sqlQuery = "DELETE from skill_groups WHERE id=" + skillGroup.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting skill group");
			}

		} catch (SQLException e) {
			System.out.println("Error on getting skill group set:" + e.getMessage());
		}
	}
	public static void updateSkillGroup(SkillGroup skillGroup) {  
		String sqlQuery = "UPDATE skill_groups SET skill_group= '" + skillGroup.getSkillGroup() 
		+ "' WHERE id = " + skillGroup.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery); 
				if(code!=1) {
					throw new SQLException("Something went wrong on updating skill group");
			}

	} catch (SQLException e) {
		System.out.println("Error on getting skill group set:" + e.getMessage());
	}
	}
	public static SkillGroup getSkillGroupById (int skillGroupId) {
		SkillGroup skillGroup = null;
		String sqlQuery = "SELECT * FROM skill_groups WHERE id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, skillGroupId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				
				skillGroup = new SkillGroup()
						.setId(results.getInt("id"))
						.setSkillGroup(results.getString("skill_group"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set: " + e.getMessage());
		}

		return skillGroup;
	}

	
	}