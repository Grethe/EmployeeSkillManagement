package ee.bcs.koolitus.employeeskillmanagement.resource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.employeeskillmanagement.dao.EmployeeSkillLevels;

public abstract class EmployeeSkillLevelsResource {

	public static List<EmployeeSkillLevels> getAllEmployeeSkillLevels() {
		List<EmployeeSkillLevels> employeeSkillLevels = new ArrayList<>();
		String sqlQuery = "SELECT * FROM employee_skill_levels";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				EmployeeSkillLevels employeeSkillLevel = new EmployeeSkillLevels().setId(results.getInt("id"))
						.setCompany(CompanyResource.getCompanyById(results.getInt("company_id")))
						.setEmployee(EmployeeResource.getEmployeeById(results.getInt("employee_id")))
						.setSkillGroup(SkillGroupResource.getSkillGroupById(results.getInt("skill_group_id")))
						.setSkill(SkillsResource.getSkillById(results.getInt("skill_id")))
						.setSkillLevel(SkillLevelResource.getSkillLevelById(results.getInt("skill_level_id")))
						.setTimeStamp(results.getTimestamp("time_stamp"));

				employeeSkillLevels.add(employeeSkillLevel);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting employee skill level set:" + e.getMessage());
		}

		return employeeSkillLevels;
	}

	/*
	 * public static void addNewNameToEmployeeSkillLevels(int employeeSkillLevelsId,
	 * Employee employee) { addNameToEmployeeSkillLevels(employeeSkillLevelsId,
	 * EmployeeResource.addEmployee(employee).getId()); }
	 * 
	 * public static void addNameToEmployeeSkillLevels(int employeeSkillLevelsId,
	 * int employeeId) { String sqlQuery =
	 * "UPDATE employee_skill_levels SET employee_id=? WHERE id=?"; try
	 * (PreparedStatement statement =
	 * DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
	 * statement.setInt(1, employeeSkillLevelsId); statement.setInt(1, employeeId);
	 * Integer code = statement.executeUpdate(); if (code != 1) { throw new
	 * SQLException("Something went wrong on adding name to employee skill levels");
	 * } } catch (SQLException e) {
	 * System.out.println("Error on adding new name to human: " + e.getMessage()); }
	 * 
	 * }
	 */

	public static void deleteEmployeeSkillLevel(EmployeeSkillLevels employeeSkillLevel) {
		String sqlQuery = "DELETE from employee_skill_levels WHERE id=" + employeeSkillLevel.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong while deleting employee skill level");
			}

		} catch (SQLException e) {
			System.out.println("Error on getting employee skill level set:" + e.getMessage());
		}
	}

	public static void updateEmployeeSkillLevel(EmployeeSkillLevels employeeSkillLevel) {
		String sqlQuery = "UPDATE employee_skill_levels SET skill_level_id='"
				// + employeeSkillLevel.getCompany().getId()
				// + "', employee_id='" + employeeSkillLevel.getEmployee().getId()
				// + "', skill_group_id='" + employeeSkillLevel.getSkillGroup().getId()
				// + "', skill_id='" + employeeSkillLevel.getSkill().getId()
				// + "', skill_level_id='"
				// NB! read 66-70 kommenteeritud,kuna employeeskilllevelsi frontendis, ehk
				// brauseris
				// muudetakse ainut skill levelit, siis võib juhtuda viga,
				// kus null'iga kirjutatakse SQLi tabelis kõik väärtused üle

				+ employeeSkillLevel.getSkillLevel().getId() + "' WHERE id = " + employeeSkillLevel.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on updating employee skill level");
			}

		} catch (SQLException e) {
			System.out.println("Error on getting employee skill level set:" + e.getMessage());
		}
	}

	public static List<EmployeeSkillLevels> addEmployeeSkillLevel(List<EmployeeSkillLevels> employeeSkillLevels) {
		for (EmployeeSkillLevels employeeSkillLevel : employeeSkillLevels) {
			String sqlQuery = "INSERT INTO employee_skill_levels (company_id, employee_id, skill_group_id, skill_id, skill_level_id) "
					+ "VALUES (?, ?, ?, ?, ?)";
			try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery,
					Statement.RETURN_GENERATED_KEYS)) {
				statement.setInt(1, employeeSkillLevel.getCompany().getId());
				statement.setInt(2, employeeSkillLevel.getEmployee().getId());
				statement.setInt(3, employeeSkillLevel.getSkillGroup().getId());
				statement.setInt(4, employeeSkillLevel.getSkill().getId());
				statement.setInt(5, employeeSkillLevel.getSkillLevel().getId());
				statement.executeUpdate();
				ResultSet resultSet = statement.getGeneratedKeys();
				while (resultSet.next()) {
					employeeSkillLevel.setId(resultSet.getInt(1));
				}
			} catch (SQLException e) {
				System.out.println("Error on adding employee skill level: " + e.getMessage());
			}
		}
		return employeeSkillLevels;
	}

}
