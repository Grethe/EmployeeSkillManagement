package ee.bcs.koolitus.employeeskillmanagement.resource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import ee.bcs.koolitus.employeeskillmanagement.dao.Employee;

public abstract class EmployeeResource {

	public static Set<Employee> getAllEmployees() {
		Set<Employee> employee = new HashSet<>();
		String sqlQuery = "SELECT * FROM employee";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Employee employees = new Employee().setId(results.getInt("id"))
						.setPersonalId(results.getString("personal_id"))
						.setFirstName(results.getString("first_name"))
						.setLastName(results.getString("last_name"))
						.setEmail(results.getString("email"))
						.setPosition(results.getString("position"))
						.setCompany(CompanyResource.getCompanyById(results.getInt("company_id")));

				employee.add(employees);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting employee set:" + e.getMessage());
		}

		return employee;
	}
	/* public static Employee addEmployee(Employee employee) {
		String sqlQuery = "INSERT INTO employee (personal_id, first_name, last_name, email, position, company_id) "
				+ "VALUES ('" 
				+ employee.getPersonalId() + "', '"
				+ employee.getFirstName() + "', '"
				+ employee.getLastName() + "', '"
				+ employee.getEmail() + "', '"
				+ employee.getPosition() + "', '"
				+ employee.getCompany().getId() + "')";

		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				employee.setId(resultSet.getInt(1));
			}

		} catch (SQLException e) {
			System.out.println("Error on adding employee: " + e.getMessage());
		}
		return employee;

	} */
	
	
	public static Employee addEmployee(Employee employee) {
		String sqlQuery = "INSERT INTO employee (personal_id, first_name, last_name, email, position, company_id) "
				+ "VALUES (?, ?, ?, ?, ?, ?)";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery,
				Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, employee.getPersonalId());
			statement.setString(2, employee.getFirstName());
			statement.setString(3, employee.getLastName());
			statement.setString(4, employee.getEmail());
			statement.setString(5, employee.getPosition());
			statement.setInt(6, employee.getCompany().getId());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				employee.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding employee: " + e.getMessage());
		}

		return employee;
	}

	
	public static void deleteEmployee(Employee employee) {
		String sqlQuery = "DELETE from employee WHERE id=" + employee.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong while deleting employee");
			}

		} catch (SQLException e) {
			System.out.println("Error on getting employee set:" + e.getMessage());
		}
	}
	public static void updateEmployee(Employee employee) {  
		String sqlQuery = "UPDATE employee SET personal_id='" + employee.getPersonalId() 
		+ "', first_name='" + employee.getFirstName() 
		+ "', last_name='" + employee.getLastName() 
		+ "', email='" + employee.getEmail() 
		+ "', position='" + employee.getPosition()  
		+ "', company_id=" + employee.getCompany().getId() 
		+ " WHERE id = " + employee.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery); 
				if(code!=1) {
					throw new SQLException("Something went wrong on updating employee");
				}

		} catch (SQLException e) {
			System.out.println("Error on getting employee set:" + e.getMessage());
		}
	}
	
	public static Employee getEmployeeById (int employeeId) {
		Employee employee = null;
		String sqlQuery = "SELECT * FROM employee WHERE id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, employeeId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				
				employee = new Employee()
						.setId(results.getInt("id"))
						.setPersonalId(results.getString("personal_id"))
						.setFirstName(results.getString("first_name"))
						.setLastName(results.getString("last_name"))
						.setEmail(results.getString("email"))
						.setPosition(results.getString("position"))
						.setCompany(CompanyResource.getCompanyById(results.getInt("company_id")));
				
						
			}
		} catch (SQLException e) {
			System.out.println("Error on getting employee set: " + e.getMessage());
		}

		return employee;
	}
	
	}

