package ee.bcs.koolitus.employeeskillmanagement.resource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.employeeskillmanagement.dao.SkillLevel;

//list of skill levels
public abstract class SkillLevelResource {
	public static List<SkillLevel> getAllSkillLevels() {
		List<SkillLevel> skillLevel = new ArrayList<>();
		String sqlQuery = "SELECT * FROM skill_level";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				SkillLevel SkillLevel = new SkillLevel().setId(results.getInt("id"))
						.setSkillLevel(results.getString("skill_level"));

				skillLevel.add(SkillLevel);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set:" + e.getMessage());
		}

		return skillLevel;
	}

	// adding new skill levels
	public static SkillLevel addSkillLevel(SkillLevel skillLevel) {
		String sqlQuery = "INSERT INTO skill_level (id, skill_level) " + "VALUES ('" + skillLevel.getId() + "', '"
				+ skillLevel.getSkillLevel() + "')";

		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				skillLevel.setId(resultSet.getInt(1));
			}

		} catch (SQLException e) {
			System.out.println("Error on adding address: " + e.getMessage());
		}
		return skillLevel;

	}
	
	//deleting skill levels
	public static void deleteSkillLevel(SkillLevel skillLevel) {
		String sqlQuery = "DELETE from skill_level WHERE id=" + skillLevel.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting skill level");
			}

		} catch (SQLException e) {
			System.out.println("Error on getting skill set:" + e.getMessage());
		}

	}
	
	//modifying skill levels
	public static void updateSkillLevel(SkillLevel skillLevel) {  
		String sqlQuery = "UPDATE skill_level SET skill_level='" + skillLevel.getSkillLevel() 
		+ "' WHERE id = " + skillLevel.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery); 
				if(code!=1) {
					throw new SQLException("Something went wrong on updating skill level");
			}

	} catch (SQLException e) {
		System.out.println("Error on getting skill level set:" + e.getMessage());
	}
	}
	//get skill level by ID
	public static SkillLevel getSkillLevelById (int skillLevelId) {
		SkillLevel skillLevel = null;
		String sqlQuery = "SELECT * FROM skill_level WHERE id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, skillLevelId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				
				skillLevel = new SkillLevel()
						.setId(results.getInt("id"))
						.setSkillLevel(results.getString("skill_level"));
						
			}
		} catch (SQLException e) {
			System.out.println("Error on getting skill level set: " + e.getMessage());
		}

		return skillLevel;
	}

}