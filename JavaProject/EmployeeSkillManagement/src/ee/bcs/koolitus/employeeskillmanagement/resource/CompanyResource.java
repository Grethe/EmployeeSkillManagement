package ee.bcs.koolitus.employeeskillmanagement.resource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import ee.bcs.koolitus.employeeskillmanagement.dao.Company;

public abstract class CompanyResource {

	public static Set<Company> getAllCompanies() {
		Set<Company> company = new HashSet<>();
		String sqlQuery = "SELECT * FROM company";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				Company skills = new Company().setId(results.getInt("id"))
						.setRegistrationCode(results.getInt("registration_code"))
						.setCompanyName(results.getString("company_name"))
						.setContactPerson(results.getString("contact_person")).setPhoneNo(results.getInt("phone_no"))
						.setEmail(results.getString("email")).setSkype(results.getString("skype"))
						.setEmployeesNo(results.getInt("employees_no")).setAddressId(results.getInt("address_id"));

				company.add(skills);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set:" + e.getMessage());
		}

		return company;
	}

	// adding new company
	public static Company addCompany(Company company) {
		String sqlQuery = "INSERT INTO company (registration_code, company_name, contact_person, phone_no, email, skype, employees_no, address_id) "
				+ "VALUES ('" + company.getRegistrationCode() + "', '"
				+ company.getCompanyName() + "', '" + company.getContactPerson() + "', '" + company.getPhoneNo()
				+ "', '" + company.getEmail() + "', '" + company.getSkype() + "', '" + company.getEmployeesNo() + "', '"
				+ company.getAddressId() + "')";

		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				company.setId(resultSet.getInt(1));
			}

		} catch (SQLException e) {
			System.out.println("Error on adding address: " + e.getMessage());
		}
		return company;

	}
	//delete company
	public static void deleteCompany(Company company) {
		String sqlQuery = "DELETE from company WHERE id=" + company.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Something went wrong while deleting company");
			}

		} catch (SQLException e) {
			System.out.println("Error on getting company set:" + e.getMessage());
		}
	}
	// modifying companies
		public static void updateCompany(Company company) {  
			String sqlQuery = "UPDATE company SET registration_code=" + company.getRegistrationCode() 
			+ ", company_name='" + company.getCompanyName() 
			+ "', contact_person='" + company.getContactPerson() 
			+ "', phone_no='" + company.getPhoneNo() 
			+ "', email='" + company.getEmail() 
			+ "', skype='" + company.getSkype() 
			+ "', employees_no='" + company.getEmployeesNo() 
			+ "', address_id='" + company.getAddressId() 
			+ "' WHERE id = " + company.getId();
			try {
				Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery); 
					if(code!=1) {
						throw new SQLException("Something went wrong on updating company");
					}

			} catch (SQLException e) {
				System.out.println("Error on getting company set:" + e.getMessage());
			}
		}
	//get company by ID	
		public static Company getCompanyById (int companyId) {
			Company company = null;
			String sqlQuery = "SELECT * FROM company WHERE id=?";
			try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
				statement.setInt(1, companyId);
				ResultSet results = statement.executeQuery();
				while (results.next()) {
					
					company = new Company()
							.setId(results.getInt("id"))
							.setRegistrationCode(results.getInt("registration_code"))
							.setCompanyName(results.getString("company_name"))
							.setContactPerson(results.getString("contact_person"))
							.setPhoneNo(results.getInt("phone_no"))
							.setEmail(results.getString("email"))
							.setSkype(results.getString("skype"))
							.setEmployeesNo(results.getInt("employees_no"))
							.setAddressId(results.getInt("address_id"));
					
				}
			} catch (SQLException e) {
				System.out.println("Error on getting company set: " + e.getMessage());
			}

			return company;
		}
}
