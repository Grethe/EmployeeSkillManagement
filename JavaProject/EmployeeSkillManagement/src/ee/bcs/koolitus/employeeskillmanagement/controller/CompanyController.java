package ee.bcs.koolitus.employeeskillmanagement.controller;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.employeeskillmanagement.dao.Company;
import ee.bcs.koolitus.employeeskillmanagement.resource.CompanyResource;

@Path("/companies")
public class CompanyController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Set<Company> getAllCompanies() {
		return CompanyResource.getAllCompanies();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Company addCompany(Company company) {
		return CompanyResource.addCompany(company);
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteCompany(Company company) {
		CompanyResource.deleteCompany(company);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateCompany(Company company) {
		CompanyResource.updateCompany(company);
	}
}
