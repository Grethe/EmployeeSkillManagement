package ee.bcs.koolitus.employeeskillmanagement.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.employeeskillmanagement.dao.SkillGroup;
import ee.bcs.koolitus.employeeskillmanagement.resource.SkillGroupResource;


@Path("/skillgroups")
public class SkillGroupController {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<SkillGroup> getAllSkillGroups() {
		return SkillGroupResource.getAllSkillGroups();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SkillGroup addSkillGroup(SkillGroup skillGroup) {
		return SkillGroupResource.addSkillGroup(skillGroup);
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteSkillGroup(SkillGroup skillGroup) {
		SkillGroupResource.deleteSkillGroup(skillGroup);
	}
	
	@PUT 
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateSkillLevel(SkillGroup skillGroup) { 
		SkillGroupResource.updateSkillGroup(skillGroup);
	}
}

