package ee.bcs.koolitus.employeeskillmanagement.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.employeeskillmanagement.dao.SkillLevel;
import ee.bcs.koolitus.employeeskillmanagement.resource.SkillLevelResource;

@Path("/skilllevels")
public class SkillLevelController {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<SkillLevel> getAllSkillLevels() {
		return SkillLevelResource.getAllSkillLevels();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SkillLevel addSkillLevel(SkillLevel skillLevel) {
		return SkillLevelResource.addSkillLevel(skillLevel);
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteSkillLevel(SkillLevel skillLevel) {
		SkillLevelResource.deleteSkillLevel(skillLevel);
	}
	@PUT 
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateSkillLevel(SkillLevel skillLevel) { 
		SkillLevelResource.updateSkillLevel(skillLevel);
	}
}