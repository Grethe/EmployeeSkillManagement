package ee.bcs.koolitus.employeeskillmanagement.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.employeeskillmanagement.dao.Skills;
import ee.bcs.koolitus.employeeskillmanagement.resource.SkillsResource;

@Path("/skills")
public class SkillsController {
	
	@GET 
	@Produces(MediaType.APPLICATION_JSON) 
	public List<Skills> getAllSkills() {
		return SkillsResource.getAllSkills(); 
	}
	
	@POST 
	@Produces(MediaType.APPLICATION_JSON)  
	@Consumes(MediaType.APPLICATION_JSON) 
	public Skills addSkill(Skills skill) {
		return SkillsResource.addSkills(skill);
	}
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteSkill(Skills skill) {
		SkillsResource.deleteSkill(skill);
	}
	@PUT 
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateSkills(Skills address) { 
		SkillsResource.updateSkills(address);
	}
}
