package ee.bcs.koolitus.employeeskillmanagement.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.employeeskillmanagement.dao.EmployeeSkillLevels;
import ee.bcs.koolitus.employeeskillmanagement.resource.EmployeeSkillLevelsResource;

@Path("/employeeskilllevels")
public class EmployeeSkillLevelsController {
	
	@GET 
	@Produces(MediaType.APPLICATION_JSON) 
	public List<EmployeeSkillLevels> getAllEmployeeSkillLevels() {
		return EmployeeSkillLevelsResource.getAllEmployeeSkillLevels(); 
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteEmployeeSkillLevel(EmployeeSkillLevels employeeSkillLevel) {
		EmployeeSkillLevelsResource.deleteEmployeeSkillLevel(employeeSkillLevel);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateEmployeeSkillLevels (EmployeeSkillLevels employeeSkillLevel) {
		EmployeeSkillLevelsResource.updateEmployeeSkillLevel(employeeSkillLevel);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<EmployeeSkillLevels> addEmployeeSkillLevel(List<EmployeeSkillLevels> employeeSkillLevel) {
		return EmployeeSkillLevelsResource.addEmployeeSkillLevel(employeeSkillLevel);
}
}