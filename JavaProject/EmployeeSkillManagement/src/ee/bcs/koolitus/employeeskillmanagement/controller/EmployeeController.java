package ee.bcs.koolitus.employeeskillmanagement.controller;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.employeeskillmanagement.dao.Employee;
import ee.bcs.koolitus.employeeskillmanagement.resource.EmployeeResource;

@Path("/employees")
public class EmployeeController {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Set<Employee> getAllEmployees() {
		return EmployeeResource.getAllEmployees();
	}

	@GET
	@Path("/{employeeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Employee getEmployeeById(@PathParam("employeeId") int employeeId) {
		return EmployeeResource.getEmployeeById(employeeId);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Employee addEmployee(Employee employee) {
		return EmployeeResource.addEmployee(employee);
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteEmployee(Employee employee) {
		EmployeeResource.deleteEmployee(employee);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateEmployee(Employee employee) {
		EmployeeResource.updateEmployee(employee);
	}
}
