package ee.bcs.koolitus.employeeskillmanagement.dao;

public class Skills {
	private int id;
	private String skill;
	private SkillGroup skillGroup;
	//private int skillGroupId;
	
	public int getId() {
		return id;
	}
	public Skills setId(int id) {
		this.id = id;
		return this;
	}
	public String getSkill() {
		return skill;
	}
	public Skills setSkill(String skill) {
		this.skill = skill;
		return this;
	}
	//public int getSkillGroupId() {
	//	return skillGroupId;
	public SkillGroup getSkillGroup() {
	return skillGroup;
	}
	//public Skills setSkillGroupId(int skillGroupId) {
	//this.skillGroupId = skillGroupId;
	public Skills setSkillGroup(SkillGroup skillGroup) {
		this.skillGroup = skillGroup;
		return this;
	}
	
//	@Override
//	public String toString() {
//		return "Skills[id=" + id + ", skill=" + skill + ", skillGroupId=" + skillGroupId + "]";
//	}

}
