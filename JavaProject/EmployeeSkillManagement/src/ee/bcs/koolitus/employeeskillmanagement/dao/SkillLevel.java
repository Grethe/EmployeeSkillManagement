package ee.bcs.koolitus.employeeskillmanagement.dao;

public class SkillLevel {
	private int id;
	private String skillLevel;
	
	
	public int getId() {
		return id;
	}
	public SkillLevel setId(int id) {
		this.id = id;
		return this;
	}
	public String getSkillLevel() {
		return skillLevel;
	}
	public SkillLevel setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
		return this;
	}
	
	@Override
	public String toString() {
		return "SkillLevel[id=" + id + ", skillLevel=" + skillLevel + "]";
	}
	

}
