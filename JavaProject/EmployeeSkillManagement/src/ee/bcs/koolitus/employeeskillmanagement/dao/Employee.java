package ee.bcs.koolitus.employeeskillmanagement.dao;

public class Employee {
	private int id;
	private String personalId;
	private String firstName;
	private String lastName;
	private String email;
	private String position;
	private Company company;

	public int getId() {
		return id;
	}

	public Employee setId(int id) {
		this.id = id;
		return this;
	}

	public String getPersonalId() {
		return personalId;
	}

	public Employee setPersonalId(String personalId) {
		this.personalId = personalId;
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public Employee setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public Employee setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public Employee setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getPosition() {
		return position;
	}

	public Employee setPosition(String position) {
		this.position = position;
		return this;
	}

	public Company getCompany() {
		return company;
	}

	public Employee setCompany(Company company) {
		this.company = company;
		return this;
	}

}
