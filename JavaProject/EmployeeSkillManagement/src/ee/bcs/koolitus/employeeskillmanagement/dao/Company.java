package ee.bcs.koolitus.employeeskillmanagement.dao;

public class Company {
	private int id;
	private int registrationCode;
	private String companyName;
	private String contactPerson;
	private int phoneNo;
	private String email;
	private String skype;
	private int employeesNo;
	private int addressId;

	public int getId() {
		return id;
	}

	public Company setId(int id) {
		this.id = id;
		return this;
	}

	public int getRegistrationCode() {
		return registrationCode;
	}

	public Company setRegistrationCode(int registrationCode) {
		this.registrationCode = registrationCode;
		return this;
	}

	public String getCompanyName() {
		return companyName;
	}

	public Company setCompanyName(String companyName) {
		this.companyName = companyName;
		return this;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public Company setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
		return this;
	}

	public int getPhoneNo() {
		return phoneNo;
	}

	public Company setPhoneNo(int phoneNo) {
		this.phoneNo = phoneNo;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public Company setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getSkype() {
		return skype;
	}

	public Company setSkype(String skype) {
		this.skype = skype;
		return this;
	}

	public int getEmployeesNo() {
		return employeesNo;
	}

	public Company setEmployeesNo(int employeesNo) {
		this.employeesNo = employeesNo;
		return this;
	}

	public int getAddressId() {
		return addressId;
	}

	public Company setAddressId(int addressId) {
		this.addressId = addressId;
		return this;
	}

	@Override
	public String toString() {
		return "Company[id=" + id 
				+ ", registrationCode=" + registrationCode 
				+ ", companyName=" + companyName 
				+ ", contactPerson=" + contactPerson 
				+ ", phoneNo=" + phoneNo 
				+ ", email=" + email
				+ ", skype=" + skype
				+ ", employeesNo=" + employeesNo
				+ ", addressId=" + addressId;
	}
}
