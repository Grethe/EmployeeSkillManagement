package ee.bcs.koolitus.employeeskillmanagement.dao;

import java.sql.Timestamp;

public class EmployeeSkillLevels {
	private int id;
	private Company company;
	private Employee employee;
	private SkillGroup skillGroup;
	private Skills skill;
	private SkillLevel skillLevel;
	private Timestamp timeStamp;
	
	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	public EmployeeSkillLevels setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
		return this;
	}
	public int getId() {
		return id;
	}
	public EmployeeSkillLevels setId(int id) {
		this.id = id;
		return this;
	}
	public Company getCompany() {
		return company;
	}
	public EmployeeSkillLevels setCompany(Company company) {
		this.company = company;
		return this;
	}
	public Employee getEmployee() {
		return employee;
	}
	public EmployeeSkillLevels setEmployee(Employee employee) {
		this.employee = employee;
		return this;
	}
	public SkillGroup getSkillGroup() {
		return skillGroup;
	}
	public EmployeeSkillLevels setSkillGroup(SkillGroup skillGroup) {
		this.skillGroup = skillGroup;
		return this;
	}
	public Skills getSkill() {
		return skill;
	}
	public EmployeeSkillLevels setSkill(Skills skill) {
		this.skill = skill;
		return this;
	}
	public SkillLevel getSkillLevel() {
		return skillLevel;
	}
	public EmployeeSkillLevels setSkillLevel(SkillLevel skillLevel) {
		this.skillLevel = skillLevel;
		return this;
	}

}
