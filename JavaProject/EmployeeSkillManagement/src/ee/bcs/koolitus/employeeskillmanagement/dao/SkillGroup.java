package ee.bcs.koolitus.employeeskillmanagement.dao;

public class SkillGroup {
	private int id;
	private String skillGroup;
	
	public int getId() {
		return id;
	}
	public SkillGroup setId(int id) {
		this.id = id;
		return this;
	}
	public String getSkillGroup() {
		return skillGroup;
	}
	public SkillGroup setSkillGroup(String skillGroup) {
		this.skillGroup = skillGroup;
		return this;
	}

}
