var allEmployeeSkillLevels;
function getAllEmployeeSkillLevels() {
	$
			.getJSON(
					"http://localhost:8080/EmployeeSkillManagement/rest/employeeskilllevels",
					function(employeeSkillLevels) {
						allEmployeeSkillLevels = employeeSkillLevels;
						var tableBody = document
								.getElementById("employeeSkillLevelsTableBody");
						var tableContent = "";
						for (var i = 0; i < employeeSkillLevels.length; i++) {
						
							var dateString = employeeSkillLevels[i].timeStamp.toLocaleString().slice(0,10).split("-");
							var timeString = employeeSkillLevels[i].timeStamp.toLocaleString().slice(11,16);
							var dateToString = dateString[2] + "." + dateString[1] + "." + dateString[0] + " " + timeString;
							
							tableContent = tableContent
									+ "<tr><td>"
									+ employeeSkillLevels[i].id
									+ "</td><td>"
									+ employeeSkillLevels[i].company.companyName
									+ "</td><td>"
									+ employeeSkillLevels[i].employee.firstName
									+ " "
									+ employeeSkillLevels[i].employee.lastName
									+ "</td><td>"
									+ employeeSkillLevels[i].skillGroup.skillGroup
									+ "</td><td>"
									+ employeeSkillLevels[i].skill.skill
									+ "</td><td>"
									+ employeeSkillLevels[i].skillLevel.skillLevel
									+ "</td><td>"
									+ dateToString
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-success' data-toggle='modal' data-target='#modifyEmployeeSkillLevels' onClick='fillEmployeeSkillLevelModifyForm("
									+ employeeSkillLevels[i].id
									+ ")'>Modify</<button>"
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-danger' onClick='deleteEmployeeSkillLevel("
									+ employeeSkillLevels[i].id
									+ ")'>Delete</<button>" + "</td></tr>";
						}
						tableBody.innerHTML = tableContent;
						$('#employeeSkillLevelTable').DataTable();

					});
}

function deleteEmployeeSkillLevel(employeeSkillLevelId) {
	var deleteEmployeeSkillLevelJson = {
		"id" : employeeSkillLevelId
	};
	$
			.ajax({
				url : "http://localhost:8080/EmployeeSkillManagement/rest/employeeskilllevels",
				type : "DELETE",
				data : JSON.stringify(deleteEmployeeSkillLevelJson),
				contentType : "application/json; charset=utf-8",
				success : function() {
					getAllEmployeeSkillLevels();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(textStatus);
				}
			});
}

function fillEmployeeSkillLevelModifyForm(employeeSkillLevelId) {
	for (var i = 0; i < allEmployeeSkillLevels.length; i++) {
		if (allEmployeeSkillLevels[i].id == employeeSkillLevelId) {
			document.getElementById("idModifyEmployeeSkillLevel").value = employeeSkillLevelId;
			document.getElementById("companyIdModifyEmployeeSkillLevel").value = allEmployeeSkillLevels[i].company.companyName;
			document.getElementById("employeeIdModifyEmployeeSkillLevel").value = allEmployeeSkillLevels[i].employee.firstName
					+ ' ' + allEmployeeSkillLevels[i].employee.lastName;
			document.getElementById("skillGroupIdModifyEmployeeSkillLevel").value = allEmployeeSkillLevels[i].skillGroup.skillGroup;
			document.getElementById("skillIdModifyEmployeeSkillLevel").value = allEmployeeSkillLevels[i].skill.skill;
			document.getElementById("skillLevelSelect").value = allEmployeeSkillLevels[i].skillLevel.id;
		}
	}
}
function changeEmployeeSkillLevels() {
	var modifyEmployeeSkillLevelJson = {
		"id" : document.getElementById("idModifyEmployeeSkillLevel").value,
		"company" : {
			"id" : document.getElementById("companyIdModifyEmployeeSkillLevel").value
		},
		"employee" : {
			"id" : document
					.getElementById("employeeIdModifyEmployeeSkillLevel").value
		},
		"skillGroup" : {
			"id" : document
					.getElementById("skillGroupIdModifyEmployeeSkillLevel").value
		},
		"skill" : {
			"id" : document.getElementById("skillIdModifyEmployeeSkillLevel").value
		},
		"skillLevel" : {
			"id" : document.getElementById("skillLevelSelect").value
		}
	}

	console.log(modifyEmployeeSkillLevelJson);
	var modifyEmployeeSkillLevel = JSON.stringify(modifyEmployeeSkillLevelJson);

	$
			.ajax({
				url : "http://localhost:8080/EmployeeSkillManagement/rest/employeeskilllevels",
				type : "PUT",
				data : modifyEmployeeSkillLevel,
				contentType : "application/json; charset=utf-8",
				success : function() {
					$("#modifyEmployeeSkillLevels").modal("hide");
					getAllEmployeeSkillLevels();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(textStatus);
				}
			});
}
