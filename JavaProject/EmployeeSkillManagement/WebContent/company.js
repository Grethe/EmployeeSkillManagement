var allCompanies;
function getAllCompanies() {
	$
			.getJSON(
					"http://localhost:8080/EmployeeSkillManagement/rest/companies",
					function(companies) {
						allCompanies = companies;
						console.log(companies);
						var tableBody = document
								.getElementById("companyTableBody");
						var tableContent = "";
						for (var i = 0; i < companies.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ companies[i].id
									+ "</td><td>"
									+ companies[i].registrationCode
									+ "</td><td>"
									+ companies[i].companyName
									+ "</td><td>"
									+ companies[i].contactPerson
									+ "</td><td>"
									+ companies[i].phoneNo
									+ "</td><td>"
									+ companies[i].email
									+ "</td><td>"
									+ checkIfNullOrUndefined(companies[i].skype)
									+ "</td><td>"
									+ companies[i].employeesNo
									+ "</td><td>"
									+ checkIfNullOrUndefined(companies[i].addressId)
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-success' data-toggle='modal' data-target='#modifyCompanies' onClick='fillCompanyModifyForm("
									+ companies[i].id
									+ ")'>Modify</<button>"
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-danger' onClick='deleteCompany("
									+ companies[i].id + ")'>Delete</<button>"
									+ "</td></tr>";
						}
						tableBody.innerHTML = tableContent;
						$('#companiesTable').DataTable();
					});
}



function checkIfNullOrUndefined(value) {
	if (value != null && value != undefined && value != "null" && value != 0
			&& value != "undefined") {
		return value;
	} else {
		return "";
	}
}

function addNewCompany() {
	var newCompanyJson = {
		"registrationCode" : document.getElementById("registrationCode").value,
		"companyName" : document.getElementById("companyName").value,
		"contactPerson" : document.getElementById("contactPerson").value,
		"phoneNo" : document.getElementById("phoneNo").value,
		"email" : document.getElementById("email").value,
		"skype" : document.getElementById("skype").value,
		"employeesNo" : document.getElementById("employeesNo").value,
		"addressId" : document.getElementById("addressId").value
	}
	console.log(newCompanyJson);
	var newCompany = JSON.stringify(newCompanyJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/companies",
		type : "POST",
		data : newCompany,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$("#addCompanies").modal("hide");
			getAllCompanies();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}
function deleteCompany(companyId) {
	var deleteCompanyJson = {
		"id" : companyId
	};
	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/companies",
		type : "DELETE",
		data : JSON.stringify(deleteCompanyJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllCompanies();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}
function fillCompanyModifyForm(companyId) {
	for (var i = 0; i < allCompanies.length; i++) {
		if (allCompanies[i].id == companyId) {
			document.getElementById("idModify2").value = companyId;
			document.getElementById("registrationCodeModify2").value = allCompanies[i].registrationCode;
			document.getElementById("companyNameModify2").value = allCompanies[i].companyName;
			document.getElementById("contactPersonModify2").value = allCompanies[i].contactPerson;
			document.getElementById("phoneNoModify2").value = allCompanies[i].phoneNo;
			document.getElementById("emailModify2").value = allCompanies[i].email;
			document.getElementById("skypeModify2").value = allCompanies[i].skype;
			document.getElementById("employeesNoModify2").value = allCompanies[i].employeesNo;
			document.getElementById("addressIdModify2").value = allCompanies[i].addressId;
		}
	}
}
function changeCompany() {
	var modifyCompanyJson = {
		"id" : document.getElementById("idModify2").value,
		"registrationCode" : document.getElementById("registrationCodeModify2").value,
		"companyName" : document.getElementById("companyNameModify2").value,
		"contactPerson" : document.getElementById("contactPersonModify2").value,
		"phoneNo" : document.getElementById("phoneNoModify2").value,
		"email" : document.getElementById("emailModify2").value,
		"skype" : document.getElementById("skypeModify2").value,
		"employeesNo" : document.getElementById("employeesNoModify2").value,
		"addressId" : document.getElementById("addressIdModify2").value
	}
	console.log(modifyCompanyJson);
	var modifyCompany = JSON.stringify(modifyCompanyJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/companies",
		type : "PUT",
		data : modifyCompany,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$("#modifyCompanies").modal("hide");
			getAllCompanies();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}	
	
	//select
	var allCompanies;
	function getAllCompanyIds() {
		$.getJSON("http://localhost:8080/EmployeeSkillManagement/rest/companies",
				function(companyIds) {
					allCompanyIds = companyIds;
					fillCompanySelect();
				});
	}

	function fillCompanySelect() {
		var companyOptions = "";
		for (var i = 0; i < allCompanyIds.length; i++) {
			companyOptions = companyOptions + "<option value='"
					+ allCompanyIds[i].id + "'>" + allCompanyIds[i].id +' - '+ allCompanyIds[i].companyName + "</option>";
		}
		document.getElementById("companyModifySelect").innerHTML = companyOptions;
		document.getElementById("companyNewSelect").innerHTML = companyOptions;
		}


