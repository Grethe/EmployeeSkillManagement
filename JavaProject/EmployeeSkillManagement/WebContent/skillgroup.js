var allSkillGroups;
function getAllSkillGroups() {
	$
			.getJSON(
					"http://localhost:8080/EmployeeSkillManagement/rest/skillgroups",
					function(skillGroup) {
						allSkillGroups = skillGroup;
						console.log(skillGroup);
						var tableBody = document
								.getElementById("skillGroupTableBody");
						var tableContent = "";
						for (var i = 0; i < skillGroup.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ skillGroup[i].id
									+ "</td><td>"
									+ skillGroup[i].skillGroup
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-success' data-toggle='modal' data-target='#modifySkillGroups' onClick='fillSkillGroupModifyForm("
									+ skillGroup[i].id
									+ ")'>Modify</<button>"
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-danger' onClick='deleteSkillGroup("
									+ skillGroup[i].id + ")'>Delete</<button>"
									+ "</td></tr>";
						}
						tableBody.innerHTML = tableContent;
					});
}


function addNewSkillGroup() {
	var newSkillGroupJson = {
		"skillGroup" : document.getElementById("skillGroup").value
	}
	console.log(newSkillGroupJson);
	var newSkillGroup = JSON.stringify(newSkillGroupJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/skillgroups",
		type : "POST",
		data : newSkillGroup,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$("#addSkillGroups").modal("hide");
			getAllSkillGroups();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("addSkillGroup" + textStatus);
		}
	});
}

function deleteSkillGroup(skillGroupId) {
	var deleteSkillGroupJson = {
		"id" : skillGroupId
	};
	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/skillgroups",
		type : "DELETE",
		data : JSON.stringify(deleteSkillGroupJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllSkillGroups();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);

		}
	});
}
function fillSkillGroupModifyForm(skillGroupId) {
	for (var i = 0; i < allSkillGroups.length; i++) {
		if (allSkillGroups[i].id == skillGroupId) {
			document.getElementById("idModify3").value = skillGroupId;
			document.getElementById("skillGroupModify3").value = allSkillGroups[i].skillGroup;
		}
	}
}
function changeSkillGroup() {
	var modifySkillGroupJson = {
		"id" : document.getElementById("idModify3").value,
		"skillGroup" : document.getElementById("skillGroupModify3").value
	}
	console.log(modifySkillGroupJson);
	var modifySkillGroup = JSON.stringify(modifySkillGroupJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/skillgroups",
		type : "PUT",
		data : modifySkillGroup,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$("#modifySkillGroups").modal("hide");
			getAllSkillGroups();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

// select 
var allSkillGroups;
function getAllSkillGroupIds() {
	$.getJSON("http://localhost:8080/EmployeeSkillManagement/rest/skillgroups",
			function(skillGroupIds) {
				allSkillGroupsIds = skillGroupIds;
				fillSkillGroupSelect();
			});
}


function fillSkillGroupSelect() {
	var skillGroupOptions = "";
	for (var i = 0; i < allSkillGroupsIds.length; i++) {
		skillGroupOptions = skillGroupOptions + "<option value='"
				+ allSkillGroupsIds[i].id + "'>" + allSkillGroupsIds[i].id +' - '+ allSkillGroupsIds[i].skillGroup + "</option>";
	}
	document.getElementById("skillGroupSelect").innerHTML = skillGroupOptions;
	document.getElementById("skillGroupSelect1").innerHTML = skillGroupOptions;
}
