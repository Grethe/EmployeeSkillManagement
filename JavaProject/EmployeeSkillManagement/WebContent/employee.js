var allEmployees;
function getAllEmployees() {
	$
			.getJSON(
					"http://localhost:8080/EmployeeSkillManagement/rest/employees",
					function(employees) {
						allEmployees = employees;
						var tableBody = document
								.getElementById("employeeTableBody");
						var tableContent = "";
						for (var i = 0; i < employees.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									// + employees[i].id + "</td><td>"
									+ employees[i].personalId
									+ "</td><td>"
									+ employees[i].firstName
									+ "</td><td>"
									+ employees[i].lastName
									+ "</td><td>"
									+ employees[i].email
									+ "</td><td>"
									+ employees[i].position
									+ "</td><td>"
									+ employees[i].company.companyName
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-warning' onClick='openSelfEvaluationForm(" 
									+ employees[i].id
									+ ")'>Self-Evaluation Form</<button>"
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-success' data-toggle='modal' data-target='#modifyEmployees' onClick='fillEmployeeModifyForm("
									+ employees[i].id
									+ ")'>Modify</<button>"
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-danger' onClick='deleteEmployee("
									+ employees[i].id + ")'>Delete</<button>"
									+ "</td></tr>";
						}
						tableBody.innerHTML = tableContent;
						$('#employeeTable').DataTable();
					});
}


function addNewEmployee() {
	var newEmployeeJson = {
		"personalId" : document.getElementById("personalId").value,
		"firstName" : document.getElementById("firstName").value,
		"lastName" : document.getElementById("lastName").value,
		"email" : document.getElementById("employeeEmail").value,
		"position" : document.getElementById("position").value,
		"company" : {
			"id" : document.getElementById("companyNewSelect").value
		}
	}

	console.log(newEmployeeJson);
	var newEmployee = JSON.stringify(newEmployeeJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/employees",
		type : "POST",
		data : newEmployee,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$("#addEmployees").modal("hide");
			getAllEmployees();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function deleteEmployee(employeeId) {
	var deleteEmployeeJson = {
		"id" : employeeId
	};
	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/employees",
		type : "DELETE",
		data : JSON.stringify(deleteEmployeeJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllEmployees();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

var changeableEmployee;
function fillEmployeeModifyForm(employeeId) {
	for (var i = 0; i < allEmployees.length; i++) {
		if (allEmployees[i].id == employeeId) {
			document.getElementById("personalIdModifyEmployee").value = allEmployees[i].personalId;
			document.getElementById("firstNameModifyEmployee").value = allEmployees[i].firstName;
			document.getElementById("lastNameModifyEmployee").value = allEmployees[i].lastName;
			document.getElementById("emailModifyEmployee").value = allEmployees[i].email;
			document.getElementById("positionModifyEmployee").value = allEmployees[i].position;
			document.getElementById("companyModifySelect").value = allEmployees[i].company.id;
			changeableEmployee = employeeId;
		}
	}
}
function changeEmployee() {
	var modifyEmployeeJson = {
		"id" : changeableEmployee,
		"personalId" : document.getElementById("personalIdModifyEmployee").value,
		"firstName" : document.getElementById("firstNameModifyEmployee").value,
		"lastName" : document.getElementById("lastNameModifyEmployee").value,
		"email" : document.getElementById("emailModifyEmployee").value,
		"position" : document.getElementById("positionModifyEmployee").value,
		"company" : {
			"id" : document.getElementById("companyModifySelect").value
		}
	}
	var modifyEmployee = JSON.stringify(modifyEmployeeJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/employees",
		type : "PUT",
		data : modifyEmployee,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$("#modifyEmployees").modal("hide");
			getAllEmployees();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

/* var addSkillsToEmployee;
function fillEmployeeSkillLevelForm(employeeId) {
	for (var i = 0; i < allEmployees.length; i++) {
		if (allEmployees[i].id == employeeId) {
			document.getElementById("selfEvaluationEmployeeName").value = allEmployees[i].firstName;
			document.getElementById("selfEvaluationEmployeeCompany").value = allEmployees[i].company.companyName;
			addSkillsToEmployee = employeeId;
		}
	}
} */

//URL query parameter
function openSelfEvaluationForm(employeeId) {
	window.location = "selfevaluationform.html?employeeId="+employeeId;
}

