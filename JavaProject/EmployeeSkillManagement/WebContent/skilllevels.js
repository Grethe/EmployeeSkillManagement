//skill levels as a list
var allSkillLevels;
function getAllSkillLevels() {
	$
			.getJSON(
					"http://localhost:8080/EmployeeSkillManagement/rest/skilllevels",
					function(skillLevels) {
						allSkillLevels = skillLevels;
						console.log(skillLevels);
						var tableBody = document
								.getElementById("skillLevelsTableBody");
						var tableContent = "";
						for (var i = 0; i < skillLevels.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ skillLevels[i].id
									+ "</td><td>"
									+ skillLevels[i].skillLevel
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-success' data-toggle='modal' data-target='#modifySkillLevels' onClick='fillSkillLevelModifyForm("
									+ skillLevels[i].id
									+ ")'>Modify</<button>"
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-danger' onClick='deleteSkillLevel("
									+ skillLevels[i].id + ")'>Delete</<button>"
									+ "</td></tr>";
						}
						tableBody.innerHTML = tableContent;
					});
}



// add new skill levels
function addNewSkillLevel() {
	var newSkillLevelJson = {
		"id" : document.getElementById("id").value,
		"skillLevel" : document.getElementById("skillLevel").value
	}
	console.log(newSkillLevelJson);
	var newSkillLevel = JSON.stringify(newSkillLevelJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/skilllevels",
		type : "POST",
		data : newSkillLevel,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$("#addSkillLevels").modal("hide");
			getAllSkillLevels();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}
// delete skill levels
function deleteSkillLevel(skillLevelId) {
	var deleteSkillLevelJson = {
		"id" : skillLevelId
	};
	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/skilllevels",
		type : "DELETE",
		data : JSON.stringify(deleteSkillLevelJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllSkillLevels();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);

		}
	});
}
// change skill levels
function fillSkillLevelModifyForm(skillLevelId) {
	for (var i = 0; i < allSkillLevels.length; i++) {
		if (allSkillLevels[i].id == skillLevelId) {
			document.getElementById("idModify1").value = skillLevelId;
			document.getElementById("skillLevelModify1").value = allSkillLevels[i].skillLevel;
		}
	}
}
function changeSkillLevel() {
	var modifySkillLevelJson = {
		"id" : document.getElementById("idModify1").value,
		"skillLevel" : document.getElementById("skillLevelModify1").value
	}
	console.log(modifySkillLevelJson);
	var modifySkillLevel = JSON.stringify(modifySkillLevelJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/skilllevels",
		type : "PUT",
		data : modifySkillLevel,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$("#modifySkillLevels").modal("hide");
			getAllSkillLevels();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

// select
var allSkillLevels;
function getAllSkillLevelIds() {
	$.getJSON("http://localhost:8080/EmployeeSkillManagement/rest/skilllevels",
			function(skillLevelIds) {
				allSkillLevelIds = skillLevelIds;
				fillSkillLevelSelect();
			});
}


function fillSkillLevelSelect() {
	var skillLevelOptions = "";
	for (var i = 0; i < allSkillLevelIds.length; i++) {
		skillLevelOptions = skillLevelOptions + "<option value='"
				+ allSkillLevelIds[i].id + "'>" + allSkillLevelIds[i].id
				+ ' - ' + allSkillLevelIds[i].skillLevel + "</option>";
	}
	document.getElementById("skillLevelSelect").innerHTML = skillLevelOptions;
}
