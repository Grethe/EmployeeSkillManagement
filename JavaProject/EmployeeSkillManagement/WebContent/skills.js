var allSkills;
function getAllSkills() {
	$
			.getJSON(
					"http://localhost:8080/EmployeeSkillManagement/rest/skills",
					function(skills) {
						allSkills = skills;
						var tableBody = document
								.getElementById("skillTableBody");
						var tableContent = "";
						for (var i = 0; i < skills.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ skills[i].id
									+ "</td><td>"
									+ skills[i].skill
									+ "</td><td>"
									+ skills[i].skillGroup.skillGroup
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-success' data-toggle='modal' data-target='#modifySkills' onClick='fillModifyForm(" 
									+ skills[i].id
									+ ")'>Modify</<button>"
									+ "</td><td>"
									+ "<button type='button' class='btn btn-outline-danger' onClick='deleteSkills("
									+ skills[i].id + ")'>Delete</<button>"
									+ "</td></tr>";
						}
						tableBody.innerHTML = tableContent;
					});
}

getAllSkills();

function addNewSkills() {
	var newSkillJson = {
		"skill" : document.getElementById("skill").value,
		"skillGroup" : {
			"id" : document.getElementById("skillGroupSelect1").value
		}
	}
	console.log(newSkillJson);
	var newSkill = JSON.stringify(newSkillJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/skills",
		type : "POST",
		data : newSkill,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$("#addSkills").modal("hide");
			getAllSkills();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function deleteSkills(skillId) {
	var deleteSkillsJson = {
		"id" : skillId
	};
	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/skills",
		type : "DELETE",
		data : JSON.stringify(deleteSkillsJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllSkills();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}
function fillModifyForm(skillId) {
	for (var i = 0; i < allSkills.length; i++) {
		if (allSkills[i].id == skillId) {
			document.getElementById("idModify").value = skillId;
			document.getElementById("skillModify").value = allSkills[i].skill;
			document.getElementById("skillGroupSelect").value = allSkills[i].skillGroup.id;
		}
	}
}
function changeSkill() {
	var modifySkillJson = {
		"id" : document.getElementById("idModify").value,
		"skill" : document.getElementById("skillModify").value,
		"skillGroup" : {"id" : document.getElementById("skillGroupSelect").value
		}
	}
	console.log(modifySkillJson);
	var modifySkill = JSON.stringify(modifySkillJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeSkillManagement/rest/skills",
		type : "PUT",
		data : modifySkill,
		contentType : "application/json; charset=utf-8",
		success : function() {
			$("#modifySkills").modal("hide");
			getAllSkills();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}
