var url = window.location.href;
var employeeId = window.location.href.split("=")[1];
console.log(employeeId);

var companyId;

function fillSelfEvaluationForm() {
	$
			.ajax({
				url : "http://localhost:8080/EmployeeSkillManagement/rest/employees/"
						+ employeeId,
				type : "GET",
				contentType : "application/json; charset=ut-f-8",
				success : function(employee) {
					var fillForm = document
							.getElementById("selfEvaluationForm");
					companyId = employee.company.id;
					document.getElementById("selfEvaluationEmployeeName").value = employee.firstName
							+ " " + employee.lastName;
					document.getElementById("selfEvaluationEmployeeCompany").value = employee.company.companyName;

				},

				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(textStatus);

				}
			});
}

fillSelfEvaluationForm();

// put self-evaluation form information into employee skill levels table
function addNewEmployeeSkillLevel() {
	var newEmployeeSkillLevelJson = [];

	var question = document.getElementsByTagName("input");
	var checkedSkillLevelIds = [];
	for (var i = 0; i < question.length; i++) {

		if (question[i].checked) {
			checkedSkillLevelIds.push(question[i]);
		}
	}

	for (var i = 0; i < checkedSkillLevelIds.length; i++) {

		newEmployeeSkillLevelJson.push({
			"employee" : {
				"id" : employeeId
			},
			"company" : {
				"id" : companyId
			},
			"skillGroup" : {
				"id" : checkedSkillLevelIds[i].id.split("_")[3]
			},
			"skill" : {
				"id" : checkedSkillLevelIds[i].id.split("_")[1]
			},
			"skillLevel" : {
				"id" : checkedSkillLevelIds[i].value
			}
		}

		);
	}

	var newEmployeeSkillLevel = JSON.stringify(newEmployeeSkillLevelJson);

	$
			.ajax({
				url : "http://localhost:8080/EmployeeSkillManagement/rest/employeeskilllevels",
				type : "POST",
				data : newEmployeeSkillLevel,
				contentType : "application/json; charset=utf-8",
				success : function() {
					getAllEmployeeSkillLevels();
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(textStatus);
				}
			});
}

// = [
// }
// {
// "employee" : {
// "id" : employeeId
// },
// "company" : {
// "id" : companyId
// },
// "skillGroup" : {
// "id" : "skill_1_skillGroup_1".split("_")[3]
// },
// "skill" : {
// "id" : "skill_1_skillGroup_1".split("_")[1]
// },
// "skillLevel" : {
// "id" : document
// .querySelector('input[id="skill_1_skillGroup_1"]:checked').value
// }
// },
// {
// "employee" : {
// "id" : employeeId
// },
// "company" : {
// "id" : companyId
// },
// "skillGroup" : {
// "id" : "skill_2_skillGroup_1".split("_")[3]
// },
// "skill" : {
// "id" : "skill_2_skillGroup_1".split("_")[1]
// },
// "skillLevel" : {
// "id" : document
// .querySelector('input[id="skill_2_skillGroup_1"]:checked').value
// }
// },
// {
// "employee" : {
// "id" : employeeId
// },
// "company" : {
// "id" : companyId
// },
// "skillGroup" : {
// "id" : "skill_3_skillGroup_1".split("_")[3]
// },
// "skill" : {
// "id" : "skill_3_skillGroup_1".split("_")[1]
// },
// "skillLevel" : {
// "id" : document
// .querySelector('input[id="skill_3_skillGroup_1"]:checked').value
// }
// },
// {
// "employee" : {
// "id" : employeeId
// },
// "company" : {
// "id" : companyId
// },
// "skillGroup" : {
// "id" : "skill_12_skillGroup_1".split("_")[3]
// },
// "skill" : {
// "id" : "skill_12_skillGroup_1".split("_")[1]
// },
// "skillLevel" : {
// "id" : document
// .querySelector('input[id="skill_12_skillGroup_3"]:checked').value
// }
// } ]

// automatically generate self-evaluation form - testing

var allSkills;
function getAllSkills() {
	$
			.getJSON(
					"http://localhost:8080/EmployeeSkillManagement/rest/skills",
					function(skills) {
						allSkills = skills;
						$
								.getJSON(
										"http://localhost:8080/EmployeeSkillManagement/rest/skilllevels",
										function(allSkillLevels) {
											var skillsSelfEvaluationForm = "";
											for (var i = 0; i < allSkills.length; i++) {
												skillsSelfEvaluationForm = skillsSelfEvaluationForm
														+ "<div><label for='skill_"
														+ allSkills[i].id
														+ "'_skillGroup_"
														+ allSkills[i].skillGroup.id
														+ "'>"
														+ allSkills[i].skill
														+ ":</label><div>"

												for (var m = 0; m < allSkillLevels.length; m++) {

													skillsSelfEvaluationForm = skillsSelfEvaluationForm
															+ "<input type='radio' id='skill_"
															+ allSkills[i].id
															+ " _skillGroup_ "
															+ allSkills[i].skillGroup.id
															+ " ' name='question_"
															+ allSkills[i].id
															+ "' value='"
															+ allSkillLevels[m].id
															+ "' > "
															+ allSkillLevels[m].skillLevel
												}

												skillsSelfEvaluationForm = skillsSelfEvaluationForm
														+ "</div></div>";

											}
											document
													.getElementById("skillsForm").innerHTML = skillsSelfEvaluationForm;
										});

					});

}

getAllSkills();

var allSkillLevels;
function getAllSkillLevels() {
	$
			.getJSON(
					"http://localhost:8080/EmployeeSkillManagement/rest/skilllevels",
					function(skillLevels) {
						allSkillLevels = skillLevels;
						var skillLevelsSelfEvaluationForm = "";
						for (var i = 0; i < skillLevels.length; i++) {
							skillLevelsSelfEvaluationForm = skillLevelsSelfEvaluationForm
									+ skillLevels[i].id
									+ "_"
									+ skillLevels[i].skillLevel + ",";
						}
					});
}
